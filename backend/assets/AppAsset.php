<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
     public $css = [
        'css/bootstrap.min.css', 
        'css/core.css', 
        'css/components.css', 
        'css/icons.css', 
        'css/pages.css', 
        'css/responsive.css'
    ];
    public $js = [
        'js/modernizr.min.js',
        'js/bootstrap.min.js', 
        'js/detect.js', 
        'js/fastclick.js',
        'js/jquery.slimscroll.js', 
        'js/jquery.blockUI.js',
        'js/waves.js',
        'js/wow.min.js',
        'js/jquery.nicescroll.js',
        'js/jquery.scrollTo.min.js',
        'js/jquery.core.js',
        'js/jquery.app.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
