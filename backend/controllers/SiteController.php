<?php

namespace backend\controllers;

use common\models\LoginForm;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use common\models\Barang;
use common\models\Supplier;
use common\models\Transaksi;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'profil'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $barang=Barang::find()->count();
        $supplier=Supplier::find()->count();
        $transaksi=Transaksi::find()->sum('harga * jumlah_item');
        return $this->render('index', ['barang'=>$barang, 'supplier'=>$supplier, 'transaksi'=>$transaksi]);


    }

    /**
     * Login action.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'blank';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionProfil()
    {
        $model = Yii::$app->user->identity;
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                //skrip khusus untuk upload file
                $model->foto = UploadedFile::getInstance($model, 'foto');
                //
                if ($model->validate()) {
                    if (isset($model->password) && (trim($model->password) != '')) {
                        $model->setPassword($model->password);
                        $model->generateAuthKey();
                    }
                    if (isset($model->foto)) {
                        $fileName = 'foto-profil/'.md5(microtime()).'.'.$model->foto->extension;
                        $model->foto->saveAs($fileName);
                        $model->image = $fileName;
                    }
                    if($model->save(false)) {
                        return $this->redirect(['site/profil']);
                    }
                }

            }    
        }
        return $this->render('profil', ['model' => $model]);
    }


}

