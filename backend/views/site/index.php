<?php

/** @var yii\web\View $this */

$this->title = 'POS';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12 col-lg-4">
        <div class="widget-bg-color-icon card-box fadeInDown animated">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="fa fa-truck text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter"><?= $supplier ?></b></h3>
                <p class="text-muted">Total Supplier</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-md-12 col-lg-4">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-warning pull-left">
                <i class="ti ti-money text-warning"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">Rp. <?= $transaksi ?></b></h3>
                <p class="text-muted">Total Transaksi</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-md-12 col-lg-4">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-pink pull-left">
                <i class="ti ti-dropbox-alt text-pink"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter"><?= $barang ?></b></h3>
                <p class="text-muted">Total Barang</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


