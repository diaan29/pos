<?php 
	//proses mengambil data user yang sudah login
	$userLogin = Yii::$app->user->identity;
	
	$this->title = 'Profil '.$userLogin->username;

	$this->params['breadcrumbs'][] = $this->title;
	use yii\widgets\ActiveForm;
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<?php $form = ActiveForm::begin() ?>
				<div class="row">
					<div class="col-md-4">
						<?= $form->field($model, 'username') ?>
					</div>
					<div class="col-md-4">
						<?= $form->field($model, 'email') ?>
					</div>
					<div class="col-md-4">
						<?= $form->field($model, 'name') ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<?= $form->field($model, 'password')->passwordInput() ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, 'foto')->fileInput(['class' => 'form-control']) ?>
					</div>
				</div>
				
				
				
				
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary">
						Update Profil
					</button>
				</div>
			<?php ActiveForm::end() ?>
		</div>
	</div>
</div>