<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Supplier $model */

$this->title = 'Update Supplier: ' . $model->nama_supplier;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
