<?php

use common\models\Transaksi;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var common\models\FilterTransaksi $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Transaksis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Create Transaksi', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="card-box">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                
                    [
                        'attribute' => 'nama_barang',
                        'value'=> function($model){
                            return $model->barang->nama_barang;
                        }
                    ],
                    'jumlah_item',
                    'harga',
                    'tanggal_transaksi',
                    [
                        'label' => 'Total harga',
                        'value' => function($model){
                            return $model->harga *$model->jumlah_item;
                        }
                    ],
                    //'bayar',
                    //'id_user',
                    [
                        'class' => ActionColumn::className(),
                        // 'urlCreator' => function ($action, Transaksi $model, $key, $index, $column) {
                        //     return Url::toRoute([$action, 'id' => $model->id]);
                        //  }
                        'buttons' => [
                                'view' => function ($url, $model)
                                {
                                    return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-icon waves-effect btn-default waves-light']);
                                },
                                'update' => function ($value='')
                                {
                                    return null;
                                },
                                'delete' => function($value='')
                                {
                                    return null;
                                }
                            ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
   

