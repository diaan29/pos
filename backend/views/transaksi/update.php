<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Transaksi $model */

$this->title = 'Update Transaksi: ' . $model->barang->nama_barang;
$this->params['breadcrumbs'][] = ['label' => 'Transaksis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transaksi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
