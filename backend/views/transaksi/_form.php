<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Barang;

/** @var yii\web\View $this */
/** @var common\models\Transaksi $model */
/** @var yii\widgets\ActiveForm $form */

// $matakuliah=ArrayHelper::map(Matakuliah::find()->all(),'id', 'nama_matkul');
// $mahasiswa=ArrayHelper::map(Mahasiswa::find()->all(), 'id', function($data){
//     return $data['nim'].' - '.$data['nama'];
// });
// $kelas=ArrayHelper::map(Kelas::find()->all(), 'id', function($data){
//     return $data['kode_kelas'].' - '.$data['nama_kelas'];
// });

$barang=ArrayHelper::map(Barang::find()->all(), 'id', 'nama_barang');

?>

<div class="transaksi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_barang')->dropDownList($barang) ?>

    <?= $form->field($model, 'jumlah_item')->textInput() ?>


    <?= $form->field($model, 'bayar')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
