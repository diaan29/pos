<?php

use common\models\Barang;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var common\models\FilterBarang $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Barangs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Create Barang', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="card-box">
             <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'nama_barang',
                    'kode_barang',
                    'harga',
                    'stok',
                    [
                        'class' => ActionColumn::className(),
                        // 'urlCreator' => function ($action, Barang $model, $key, $index, $column) {
                        //     return Url::toRoute([$action, 'id' => $model->id]);
                        //  }
                        'buttons' => [
                                'view' => function ($url, $model)
                                {
                                    return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-icon waves-effect btn-default waves-light']);
                                },
                                'update' => function ($value='')
                                {
                                    return null;
                                },
                                'delete' => function($value='')
                                {
                                    return null;
                                }
                            ]
                    ],
                ],
            ]); ?>

        </div>
    </div>
</div>
    
