<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Supplier;
use yii\helpers\ArrayHelper;

$supplier=ArrayHelper::map(Supplier::find()->all(), 'id', 'nama_supplier');

/** @var yii\web\View $this */
/** @var common\models\Barang $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="barang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_barang')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kode_barang')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'harga')->textInput() ?>

    <?= $form->field($model, 'stok')->textInput() ?>

    <?= $form->field($model, 'id_supplier')->dropDownList($supplier) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
