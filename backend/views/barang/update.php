<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Barang $model */

$this->title = 'Update Barang: ' . $model->nama_barang;
$this->params['breadcrumbs'][] = ['label' => 'Barangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="barang-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
