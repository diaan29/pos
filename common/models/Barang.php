<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "barang".
 *
 * @property int $id
 * @property string $nama_barang
 * @property string $kode_barang
 * @property float $harga
 * @property int $stok
 * @property int $id_supplier
 *
 * @property Supplier $supplier
 * @property Transaksi[] $transaksis
 */
class Barang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_barang', 'kode_barang', 'harga', 'stok', 'id_supplier'], 'required'],
            [['harga'], 'number'],
            [['stok', 'id_supplier'], 'integer'],
            [['nama_barang'], 'string', 'max' => 100],
            [['kode_barang'], 'string', 'max' => 50],
            [['nama_barang', 'kode_barang'], 'unique', 'targetAttribute' => ['nama_barang', 'kode_barang']],
            [['id_supplier'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::class, 'targetAttribute' => ['id_supplier' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_barang' => 'Nama Barang',
            'kode_barang' => 'Kode Barang',
            'harga' => 'Harga',
            'stok' => 'Stok',
            'id_supplier' => 'Id Supplier',
        ];
    }

    /**
     * Gets query for [[Supplier]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::class, ['id' => 'id_supplier']);
    }

    /**
     * Gets query for [[Transaksis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksis()
    {
        return $this->hasMany(Transaksi::class, ['id_barang' => 'id']);
    }
}
