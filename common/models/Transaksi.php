<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transaksi".
 *
 * @property int $id
 * @property int $id_barang
 * @property int $jumlah_item
 * @property float $harga
 * @property string $tanggal_transaksi
 * @property float $bayar
 * @property int $id_user
 *
 * @property Barang $barang
 * @property User $user
 */
class Transaksi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang', 'jumlah_item', 'tanggal_transaksi', 'bayar', 'id_user'], 'required'],
            [['id_barang', 'jumlah_item', 'id_user'], 'integer'],
            [['harga', 'bayar'], 'number'],
            [['tanggal_transaksi'], 'safe'],
            [['id_barang'], 'exist', 'skipOnError' => true, 'targetClass' => Barang::class, 'targetAttribute' => ['id_barang' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_barang' => 'Id Barang',
            'jumlah_item' => 'Jumlah Item',
            'harga' => 'Harga',
            'tanggal_transaksi' => 'Tanggal Transaksi',
            'bayar' => 'Bayar',
            'id_user' => 'Id User',
        ];
    }

    /**
     * Gets query for [[Barang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(Barang::class, ['id' => 'id_barang']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }
}
