<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transaksi;

/**
 * FilterTransaksi represents the model behind the search form of `common\models\Transaksi`.
 */
class FilterTransaksi extends Transaksi
{
    /**
     * {@inheritdoc}
     */
    public $nama_barang;
    public function rules()
    {
        return [
            [['id', 'jumlah_item', 'id_user'], 'integer'],
            [['harga', 'bayar'], 'number'],
            [['tanggal_transaksi', 'nama_barang'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaksi::find()->joinWith(['barang']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'jumlah_item' => $this->jumlah_item,
            'harga' => $this->harga,
            'tanggal_transaksi' => $this->tanggal_transaksi,
            'bayar' => $this->bayar,
        ]);

        $query->andFilterWhere([
            'like', 
            'barang.nama_barang', 
            $this->nama_barang
        ]);

        return $dataProvider;
    }
}
